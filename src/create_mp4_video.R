library(foreign)
library(stringr)
library(dplyr)
library(ggplot2)
library(scales)
library(animation)

rm(list = ls())

# possibly set your working directory here if needed! 
# setwd()...

## 0. Data Cleaning and Preparation ## 
# read like data
likes <- read.csv('data/fb_likes.csv', stringsAsFactors = F, sep = ";")

# extract date in YYYY-mm-dd format from Posix 
likes$date <-  str_replace_all(likes$date, "(\\d{4}-\\d{2}-\\d{2}).+", "\\1") # a horrible regex, but it works! ;) 
likes$date <- as.Date(likes$date, format = "%Y-%m-%d") # format as Date 

# delete double observations - whatever the scraper was doing there extracting two obs per day?!
# in either way, it does not matter if we delete one.
likes <- unique(likes, by = "date") 

# sort data frame by increasing date just to make sure 
likes <- likes %>%
  arrange(date)

# time variable for easier subsetting later
likes$time <- 1:nrow(likes)

## 1. Creation of the video ##

# extract minima and maxima of count (y axis) and date (x axis) to keep the axes limits constant for the plots 
miny <- min(likes$count)
maxy <- max(likes$count)
minx <- min(likes$date)
maxx <- max(likes$date)

# define plot function that takes a cutoff value  as input and plots the data up to this cutoff 
# later, we need to iteratively go through the data frame to "build" up the graph 
plot <- function(cutoff){
  if(cutoff <= nrow(likes)){
    
    # create subset of data until cutoff value 
    likes_sub <- likes[likes$time <= cutoff, ]
    
    # create plot 
    p <- ggplot(likes_sub, aes(x = date, y = count))+
      geom_area(stat = "identity", colour = "#92DE3F", fill = "#92DE3F")+ # fill the area 
      scale_x_date("", limits = c(minx, maxx), date_breaks = "month", date_labels = "%b", expand = c(0,0))+ # x axis 
      scale_y_continuous("# amazing people liking us", limits = c(miny, maxy), expand = c(0,0)) # y axis
  }else{
    
    p <- ggplot(likes, aes(x = date, y = count))+
      geom_area(stat = "identity", colour = "#92DE3F", fill = "#92DE3F")+ # fill the area 
      scale_x_date("", limits = c(minx, maxx), date_breaks = "month", date_labels = "%b", expand = c(0,0))+ # x axis 
      scale_y_continuous("# amazing people liking us", limits = c(miny, maxy), expand = c(0,0))+ # y axis 
      geom_text(aes(label = "THANK YOU! <3", x = as.Date("2016-03-12"), y = 150,  angle = 30),  size = 16, colour = "#3863A2")
  }
  
  
  # add theme 
  p <- p + theme(panel.background = element_blank(), # no background grid lines or colour 
                 axis.line.x = element_line(size = 1, linetype = "solid", colour = "#3863A2"), # axis line specifics 
                 axis.line.y = element_line(size = 1, linetype = "solid", colour = "#3863A2"),
                 axis.ticks.x = element_line(colour = "#3863A2"), # ticks in blue 
                 axis.ticks.y = element_line(colour = "#3863A2"),
                 axis.text.x =  element_text(colour = "#3863A2"), # tick labels in blue 
                 axis.text.y =  element_text(colour = "#3863A2"), 
                 axis.title.y = element_text(colour = "#3863A2")) # axis label in blue   
  
  # print the plot to add it to the device
  print(p)
}

#function to iterate over the full span of x-values and apply plot function torows 1:i of likes 
loop_x <- function(thank_you_plots) {
  lapply(seq(1,nrow(likes)+thank_you_plots,1), function(i) {
    plot(i)
  })
}

#save all iterations into one mp4
# overall, this is not really efficient (it's really not!) but it's the best my r skills 
# in combination with my r skills could come up with ;) 
saveVideo(loop_x(50), interval = .02, movie.name="correlaid_likes_over_time.mp4")

